
function adicionarAoCarrinho(id) {
  var carrinho = sessionStorage.getItem('carrinho') || {};
  if (typeof(carrinho) !== 'object') {
    carrinho = JSON.parse(carrinho);
  }

  if (!carrinho[id]) {
    carrinho[id] = { qtde: 1 };
  } else {
    carrinho[id].qtde++;
  }

  sessionStorage.setItem('carrinho', JSON.stringify(carrinho));
}

function removerDoCarrinho(id) {
  var carrinho = sessionStorage.getItem('carrinho') || {};
  if (typeof(carrinho) !== 'object') {
    carrinho = JSON.parse(carrinho);
  }
  delete carrinho[id];
  sessionStorage.setItem('carrinho', JSON.stringify(carrinho));
}
