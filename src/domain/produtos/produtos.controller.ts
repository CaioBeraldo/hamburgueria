import { Controller, Get, Req, Res } from '@nestjs/common';
import { ProdutosService } from './produtos.service';

@Controller('produtos')
export class ProdutosController {
  constructor(private readonly produtosService: ProdutosService) {}

  @Get('comprar')
  getComprarProdutos(@Req() req, @Res() res) {
    res.render('cliente/cadastro_produto', {
    });
  }

}
