import { Module } from '@nestjs/common';
import { ProdutosController } from './produtos.controller';
import { ProdutosService } from './produtos.service';
import { produtosProvider } from './produtos.provider';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [ProdutosController],
  providers: [ProdutosService, ...produtosProvider],
  exports: [ProdutosService, ...produtosProvider],
})
export class ProdutosModule {}
