import { Connection } from 'mongoose';
import { ProdutosSchema } from './produtos.schema';
import { CONSTANTS } from 'src/constants';

export const produtosProvider = [
  {
    provide: CONSTANTS.SCHEMAS.PRODUTOS.TOKEN,
    useFactory: (connection: Connection) =>
      connection.model(CONSTANTS.SCHEMAS.PRODUTOS.MODEL, ProdutosSchema),
    inject: [CONSTANTS.DATABASE.TOKEN],
  },
];
