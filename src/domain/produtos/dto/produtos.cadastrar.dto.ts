import { IsMongoId, IsNumber, IsInt, IsJSON, Min, MaxLength, MinLength, IsString } from 'class-validator';

export class ProdutosCadastrarPayload {
  @IsString({ message: 'Nome do produto inválido' })
  @MinLength(3, { message: 'Nome muito curto' })
  @MaxLength(100, { message: 'Nome muito longa' })
  readonly nome: string;

  @IsString({ message: 'Descrição do produto inválido' })
  @MinLength(3, { message: 'Descrição muito curta' })
  @MaxLength(100, { message: 'Descrição muito longa' })
  readonly descricao: string;

  @Min(1)
  readonly valor: number;

  readonly imagem: Buffer;
}
