import { Document } from 'mongoose';

export interface IProduto extends Document {
  readonly id: { type: string };
  readonly nome: { type: string };
  readonly descricao: { type: string };
  readonly valor: { type: number };
  readonly imagem: { type: string };
}
