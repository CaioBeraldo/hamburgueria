import { Schema } from 'mongoose';

export const ProdutosSchema = new Schema({
  nome: { type: String },
  descricao: { type: String },
  valor: { type: Number },
  imagem: { data: Buffer, contentType: String },
});
