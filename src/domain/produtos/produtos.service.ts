import { Injectable, Inject } from '@nestjs/common';
import { CONSTANTS } from 'src/constants';
import { IProduto } from './produtos.interface';
import { Model } from 'mongoose';
import { ProdutosCadastrarPayload } from './dto/produtos.cadastrar.dto';

@Injectable()
export class ProdutosService {
  constructor(@Inject(CONSTANTS.SCHEMAS.PRODUTOS.TOKEN) private readonly produtosService: Model<IProduto>) {}

  findAll(): Promise<IProduto[]> {
    return this.produtosService.find({});
  }

  findById(id): Promise<IProduto> {
    return this.produtosService.findById(id);
  }

  create(payload: ProdutosCadastrarPayload): Promise<IProduto> {
    const produto = new this.produtosService(payload);
    // product.img1.data = fs.readFileSync(imgPath);
    // product.img1.contentType = 'jpg';
    return produto.save();
  }

  remove(id): Promise<any> {
    return this.produtosService.findOneAndDelete(id);
  }

}
