import { Controller, Get, Res, Req, Body, Post, HttpStatus, Session } from '@nestjs/common';
import { ProdutosService } from '../produtos/produtos.service';
import { ClientesService } from './clientes.service';
import { ICliente } from './clientes.inteface';

@Controller('/')
export class ClientesController {
  constructor(
    private readonly produtoService: ProdutosService,
    private readonly clienteService: ClientesService) {}

  @Get()
  async getProdutos(@Req() req, @Res() res) {
    const produtos = await this.produtoService.findAll();
    res.render('cliente/listagem_produtos', {
      produtos,
    });
  }

  @Get('/login')
  getLogin(@Req() req, @Res() res) {
    res.render('cliente/login', {
      pedidos: this.produtoService.findAll(),
    });
  }

  @Post('/login')
  async postLogin(@Req() req, @Res() res, @Session() session, @Body() payload) {
    const cliente = await this.clienteService.findByEmail(payload.email);
    session.cliente = cliente;

    const produtos = await this.produtoService.findAll();
    res.render('cliente/listagem_produtos', {
      produtos,
    });
  }

  @Get('/registrar')
  async getRegistrar(@Req() req, @Res() res) {
    res.render('cliente/registrar', {});
  }

  @Post('/registrar')
  async postRegistrar(@Req() req, @Res() res, @Session() session, @Body() payload) {
    let cliente = await this.clienteService.findByEmail(payload.email);

    if (cliente) {
      return res.render('cliente/login', {
        email: payload.email,
        mensagem: 'Usuário já cadastrado.',
      });
    }

    cliente = await this.clienteService.create(payload);

    this.postLogin(req, res, cliente, session);
  }

  @Get('/pedidos')
  getPedidos(@Req() req, @Res() res) {
    res.render('cliente/listagem_pedidos', {
      pedidos: this.produtoService.findAll(),
    });
  }

  /*
  @Get('/carrinho')
  async getCarrinho(@Req() req, @Res() res, @Session() session, @Body() payload) {
    if (!session.cliente) {
      return this.getRegistrar(req, res);
    }

    const cliente: ICliente = await this.clienteService.findById(session.cliente.id);

    res.render('cliente/carrinho', {
      cliente,
    });
  }

  @Post('/carrinho')
  async postCarrinho(@Res() res, @Req() req, @Body() payload) {
    // const cliente: ICliente[] = await this.clienteService.findAll();
    const cliente = {
      carrinho: [],
    };
    cliente.carrinho = await this.produtoService.findAll();

    // const carrinho = sessionStorage.getItem('carrinho');
    res.render('cliente/carrinho', {
      cliente,
    });
  }
  */

}
