import { Connection } from 'mongoose';
import { CONSTANTS } from 'src/constants';
import { ClientesSchema } from './clientes.schema';

export const clientesProvider = [
  {
    provide: CONSTANTS.SCHEMAS.CLIENTES.TOKEN,
    useFactory: (connection: Connection) =>
      connection.model(CONSTANTS.SCHEMAS.CLIENTES.MODEL, ClientesSchema),
    inject: [CONSTANTS.DATABASE.TOKEN],
  },
];
