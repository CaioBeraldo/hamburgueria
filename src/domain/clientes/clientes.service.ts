import { Injectable, Inject } from '@nestjs/common';
import { CONSTANTS } from 'src/constants';
import { Model } from 'mongoose';
import { ICliente } from './clientes.inteface';

@Injectable()
export class ClientesService {
  constructor(@Inject(CONSTANTS.SCHEMAS.CLIENTES.TOKEN) private readonly clientesService: Model<ICliente>) {}

  findAll(): Promise<ICliente[]> {
    return this.clientesService.find({});
  }

  findById(id): Promise<ICliente> {
    return this.clientesService.findById(id);
  }

  findByEmail(email): Promise<ICliente> {
    return this.clientesService.findOne({ email }).lean();
  }

  create(payload): Promise<ICliente> {
    const cliente = new this.clientesService(payload);
    return cliente.save();
  }

  remove(id): Promise<any> {
    return this.clientesService.findOneAndDelete(id);
  }

}
