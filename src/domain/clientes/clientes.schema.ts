import { Schema } from 'mongoose';

export const ClientesSchema = new Schema({
  nome: { type: String },
  email: { type: String },
  carrinho: { type: Array, default: [] },
});
