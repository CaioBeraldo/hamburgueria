import { Document } from 'mongoose';
import { IProduto } from '../produtos/produtos.interface';

export interface ICliente extends Document {
  readonly id: { type: string };
  readonly nome: { type: string };
  readonly email: { type: string };
  readonly carrinho: { type: IProduto[] };
}
