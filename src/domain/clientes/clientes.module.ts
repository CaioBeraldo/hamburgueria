import { Module } from '@nestjs/common';
import { ClientesController } from './clientes.controller';
import { ProdutosModule } from '../produtos/produtos.module';
import { clientesProvider } from './clientes.provider';
import { ClientesService } from './clientes.service';

@Module({
  imports: [ProdutosModule],
  controllers: [ClientesController],
  providers: [ClientesService, ...clientesProvider],
  exports: [],
})
export class ClientesModule {}
