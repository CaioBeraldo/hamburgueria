import { Controller, Get, Req, Res, Post, HttpStatus, Body, Param } from '@nestjs/common';
import { ProdutosService } from '../produtos/produtos.service';
import { ProdutosCadastrarPayload } from '../produtos/dto/produtos.cadastrar.dto';

@Controller('gerente')
export class GerenteController {
  constructor(private readonly produtosService: ProdutosService) {}

  @Get()
  getHome(@Req() req, @Res() res) {
    res.render('gerente/inicio');
  }

  @Get('produtos')
  async getProdutos(@Req() req, @Res() res) {
    res.render('gerente/listagem_produtos', {
      produtos: await this.produtosService.findAll(),
    });
  }

  @Get('produtos/cadastrar')
  async getCadastrarProdutos(@Req() req, @Res() res) {
    res.render('gerente/cadastro_produto', {
    });
  }

  @Post('produtos/cadastrar')
  async postCadastrarProdutos(@Req() req, @Res() res, @Body() payload: ProdutosCadastrarPayload) {
    const produto = await this.produtosService.create(payload);

    this.getProdutos(req, res);
  }

  @Get('produtos/confirmarExclusao/:id')
  async getConfirmarExclusao(@Req() req, @Res() res, @Param('id') id) {
    const produto = await this.produtosService.findById(id);
    res.render('gerente/excluir_produto', {
      produto,
    });
  }

  @Post('produtos/confirmarExclusao/:id')
  async postConfirmarExclusao(@Req() req, @Res() res, @Param('id') id) {
    await this.produtosService.remove(id);

    this.getProdutos(req, res);
  }

  @Get('pedidos')
  async getPedidos(@Req() req, @Res() res) {
    let pedidos = await this.produtosService.findAll();

    if (pedidos.length === 0) {
      pedidos = undefined;
    }

    res.render('gerente/listagem_pedidos', {
      pedidos,
    });
  }

  @Get('pedidos/alterar')
  getAlterarPedidos(@Req() req, @Res() res) {
    return '';
  }
}
