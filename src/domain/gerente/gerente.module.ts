import { Module } from '@nestjs/common';
import { GerenteController } from './gerente.controller';
import { ProdutosModule } from '../produtos/produtos.module';

@Module({
  imports: [ProdutosModule],
  controllers: [GerenteController],
})
export class GerenteModule {}
