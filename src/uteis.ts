import * as path from 'path';
import * as fs from 'fs';
import * as hbs from 'hbs';
import { Logger } from '@nestjs/common';

export const registerHbsTemplates = (dirName) => {
  const dir = path.join(__dirname, dirName);

  const walkSync = (directory, filelist = []) => {
    fs.readdirSync(directory).forEach(file => {

      filelist = fs.statSync(path.join(directory, file)).isDirectory()
        ? walkSync(path.join(directory, file), filelist)
        : filelist.concat(path.join(directory, file));
    });

    return filelist;
  };

  const files = walkSync(dir);

  if (files.length > 0) {
    files.forEach((filename) => {
      const matches = /^([^.]+).hbs$/.exec(path.basename(filename));

      if (!matches) {
        return;
      }

      const name = matches[1];
      const template = fs.readFileSync(filename, 'utf8');

      Logger.log(name);
      hbs.registerPartial(name, template);
    });
  }
};
