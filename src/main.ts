import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { urlencoded, json } from 'body-parser';
import * as hbs from 'express-hbs';
import * as session from 'express-session';
import * as cookieParser from 'cookie-parser';

function relative(dirName) {
  return `${__dirname}/${dirName}`;
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(json());
  app.use(urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(session({
    secret: 'ZXhwcmVzcyBzZXNzaW9u',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000, secure: true },
  }));

  // https://github.com/barc/express-hbs/blob/master/example/views/layout/fruits.hbs
  app.engine('hbs', hbs.express4({
    partialsDir: [
      relative('views/gerente'),
      relative('views/cliente'),
      relative('views/shared'),
    ],
    layoutsDir: [
      relative('views/layout'),
    ],
    defaultLayout: relative('views/layout/default.hbs'),
  }));
  app.set('view engine', 'hbs');
  app.set('views', relative('views'));
  app.useStaticAssets(relative('public'));

  await app.listen(3000);
}
bootstrap();
