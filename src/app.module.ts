import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ProdutosModule } from './domain/produtos/produtos.module';
import { ClientesModule } from './domain/clientes/clientes.module';
import { GerenteModule } from './domain/gerente/gerente.module';

@Module({
  imports: [ProdutosModule, ClientesModule, GerenteModule],
  controllers: [AppController],
  providers: [],
  // providers: [AppService],
})
export class AppModule {}
