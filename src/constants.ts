
export const CONSTANTS = {
  DATABASE: {
    TOKEN: 'DataBaseToken',
    CONNECTION: 'mongodb://localhost/hamburgueria',
  },
  SCHEMAS: {
    PRODUTOS: {
      TOKEN: 'ProdutosToken',
      MODEL: 'Produtos',
    },
    CLIENTES: {
      TOKEN: 'ClientesToken',
      MODEL: 'Clientes',
    },
  },
};
