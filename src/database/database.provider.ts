import * as mongoose from 'mongoose';
import { CONSTANTS } from 'src/constants';

// MongooseModule.forRoot('mongodb://localhost/usuarios', { useNewUrlParser: true }),
// MongooseModule.forFeature([{ name: 'Usuario', schema: UsuariosSchema }]),
export const databaseProviders = [
  {
    provide: CONSTANTS.DATABASE.TOKEN,
    useFactory: async (): Promise<typeof mongoose> =>
      await mongoose.connect(CONSTANTS.DATABASE.CONNECTION, { useNewUrlParser: true }),
  },
];
